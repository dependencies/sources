
#include <plustache/template.hpp>
#include <plustache/context.hpp>

#include <string>
#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
    template_t t;
    string str_template;
    str_template = "text {{foo}} text";
    Context ctx;
    ctx.add("foo", "bla");
    string s;
    s = t.render(str_template, ctx);
    cout << s << endl;
    return 0;
}
