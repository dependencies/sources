======
Raptor
======

Summary
=======

Official/Original website : http://librdf.org/
Sources (archive) : http://download.librdf.org/source/raptor2-2.0.9.tar.gz
tag/commit used : v2.0.9
License : LGPL 2.1, GPL 2 or Apache 2
