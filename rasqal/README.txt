======
Rasqal
======

Summary
=======

Official/Original website : http://librdf.org/
Sources (archive) : http://download.librdf.org/source/rasqal-0.9.30.tar.gz
tag/commit used : v0.9.30
License : LGPL 2.1, GPL 2 or Apache 2
